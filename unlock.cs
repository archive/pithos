﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace Pithos
{
    public partial class unlock : Form {
        private List<string> validUserNames = privateFunctions.validUserNames();
        private List<string> validMachineNames = privateFunctions.validMachineNames();
        private List<char> licenseChar = new List<char>();

        public unlock() {
            InitializeComponent();
        }

        private void loginSuccessful() {
            this.Hide();
            Program.MW.Show();
            licenceInput.Text = "";
            unlockButton.Text = "Unlock";
        }

        private void checkWorkSpaceValidations() {
            string machineName = Environment.MachineName;
            string domainName = Environment.UserDomainName;
            string userName = Environment.UserName;
            if (validUserNames.Contains(userName) || validMachineNames.Contains(machineName) || validMachineNames.Contains(domainName)) {
                MessageBox.Show("Valid Computer Credentials have been detected. Automatic Login Initiated.", "Computer Confirmed", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            loginSuccessful();

        }
        private void button1_Click(object sender, EventArgs e) {//validation
            unlockButton.Text = "Validating...";
            //checkWorkSpaceValidations();
            if (privateFunctions.checkHardCodes(licenceInput.Text)) {
                MessageBox.Show("You have entered hard-coded login credentials. Access Granted.", "Access Granted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                loginSuccessful();
            }
            else
            {
                MessageBox.Show("Incorrent Login. Access Denied", "Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                unlockButton.Text = "Unlock";
            }
        }

        private void infoLabel_Click(object sender, EventArgs e) { System.Diagnostics.Process.Start("http://theorangeone.net/pithos"); }

        private void licenceInput_TextChanged(object sender, EventArgs e)
        {
            System.Random rnd = new System.Random();
            rnd.Next(licenseChar.Count);

        }
    }
}
