﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Diagnostics;
namespace Pithos
{
    class decode
    {
        public decode() { }
        public string injectVariables(string text) {
            text = text.Replace("$user", Environment.UserName);
            return text;
        }
        public bool command(string text) {
            string[] args = text.Substring(1).Split(' ');//splits string and removed character

            if (args[0] == "lock") { //locks the program
                Program.MW.display("Locking Software...", Color.Red);
                Program.MW.adminMode = false;
                Program.MW.Hide();
                Program.UL.Show();
                return true;
            }
            else if (args[0] == "hash") { //hash things
                for (int i = 1; i < args.Length; i++) { Program.MW.display("String: " + args[i] + "    Hash: " + tools.hash(args[0]), Color.Green); }
                return true;
            }
            else if (args[0] == "shutdown") { Process.Start("shutdown /s /t 0"); return true; } //shutdown computer

            else if (args[0] == "echo") { Program.MW.display(args[1], Color.LimeGreen); return true; } //display content

            else if (args[0] == "override" && args[1] == "¦") { Program.MW.adminMode = true; Program.MW.display("Admin Mode had been enabled.", Color.Orange); return true; } //activate admin


            return false;
        }
    }
}
