﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pithos
{
    public partial class mainWindow : Form
    {
        private decode decoder = new decode();
        public bool adminMode = false;
        public mainWindow()
        {
            InitializeComponent();
        }
        public void display(string text, Color colour) {
            /*  Message Color Codes:
             *      Light Green: User Messages (Mainly Echo)
             *      Orange: Admin Commands
             *      Black: General text / Date
             *      Red: Error Messages
             *      Blue: Messages from Program
             */
            if (output.TextLength != 0) { output.AppendText("\n[" + DateTime.Now.ToString("hh:mm:ss") + "] "); } //so that the first line isnt a new line for no reason. (although its extra code)
            else { output.AppendText("[" + DateTime.Now.ToString("hh:mm:ss") + "] "); }
            int length = output.TextLength;
            text = decoder.injectVariables(text);
            output.AppendText(text);
            output.SelectionStart = length;
            output.SelectionLength = text.Length;
            output.SelectionColor = colour;
            output.SelectionStart = length + text.Length;
            output.SelectionColor = Color.White;
        }
        private void mainWindow_Load(object sender, EventArgs e)
        {
            display("Currently Logged in as: " + Environment.UserDomainName + "\\" + Environment.UserName, Color.White);
        }

        private void executeButton_Click(object sender, EventArgs e) {
            bool valid = false;
            string input = inputBox.Text;
            if (input == "") { display("Error: No command given.", Color.Red); return; } //if its empty, dont bother decoding
            string initChar = input.Substring(0, 1); //gets the first character in the input
            string[] args = input.Substring(1).Split(' '); //splits the input by spaces, and removes init character
            inputBox.Text = ""; //clear the contents after execution
            try
            {
                switch (initChar)
                {
                    case "/":
                        if (args[0] == "exec") //runs a pithos program
                        {
                            valid = pithosPrograms.decodeInput(input);
                            return;
                        }
                        else if (args[0] == "start") //runs a program (using cmd)
                        {

                        }
                        else { valid = decoder.command(input); } //decode other commands
                        break;

                    case "¦": //admin commands
                        if (!adminMode) {
                            display("Access Denied: Admin commands are currently disabled.", Color.Orange);
                            return;
                        }
                        valid = privateFunctions.adminCommands(input);
                        break;

                    default:
                        break;
                }
            } catch {
                displayError(input);
                return;
            }
            if (!valid) { displayError(input); }
        }

        private void displayError(string input)
        {
            display("Error: Command '" + input + "' is not recognised.", Color.Red);
        }

        private void mainWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }
    }
}
