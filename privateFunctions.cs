﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Drawing;
namespace Pithos
{
    class privateFunctions {
        public static bool adminCommands(string input)
        {
            return false;
        }

        public static List<string> validUserNames() {
            List<string> valids = new List<string>();
            valids.Add("Jake");
            valids.Add("13HowardJS03");
            return valids;
        }

        public static List<string> validMachineNames()
        {
            List<string> valids = new List<string>();
            valids.Add(")");
            return valids;
        }
        public static bool checkHardCodes(string input)
        {
            string hash = tools.hash(input).ToLower();
            switch (hash)
            {
                case "747ec194be4358b3d54b3a29f5a20546":
                    return true;
                default:
                    return false;
            }
        }
    }
}
